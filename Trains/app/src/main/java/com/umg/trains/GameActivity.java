package com.umg.trains;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.umg.trains.battle.Battle;
import com.umg.trains.battle.IBattle;
import com.umg.trains.battle.Player;
import com.umg.trains.db.Results;
import com.umg.trains.trains.Train;
import com.umg.trains.trains.TrainStore;
import com.umg.trains.utils.Constants;
import com.umg.trains.utils.Utils;

import java.time.LocalDateTime;

public class GameActivity extends AppCompatActivity {

    Battle battle;
    Constants.DIFFICULTY difficulty;

    IBattle actions = new IBattle() {
        @Override
        public void onBattleEnd(int winner) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                addResultToDb(winner);
            }
            String winText = winner == 0 ? "Wygrałeś!" : "Przegrałeś!";
            Toast.makeText(getApplicationContext(), winText, Toast.LENGTH_LONG).show();
            goToMainScreen();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        Train playerTrain = TrainStore.getPlayerTrain();
        Train enemyTrain = TrainStore.trains.get(Utils.getRandomNumber(0, TrainStore.trains.size() - 1));

        ImageView enemyTrainImage = findViewById(R.id.enemyTrain);
        enemyTrainImage.setImageResource(enemyTrain.getImage());

        ImageView playerTrainImage = findViewById(R.id.playerTrain);
        playerTrainImage.setImageResource(playerTrain.getImage());

        ProgressBar playerHPBar = findViewById(R.id.playerHPBar);
        ProgressBar playerArmour = findViewById(R.id.playerArmourBar);
        ProgressBar playerLoading = findViewById(R.id.playerLoading);

        ProgressBar enemyHPBar = findViewById(R.id.enemyHPBar);
        ProgressBar enemyLoading = findViewById(R.id.enemyLoading);
        ProgressBar enemyArmour = findViewById(R.id.enemyArmourBar);

        Player player = new Player(playerHPBar, playerArmour, playerLoading, playerTrain);
        Player enemy = new Player(enemyHPBar, enemyArmour, enemyLoading, enemyTrain);

        difficulty = Utils.readDifficulty(getApplicationContext());
        battle = new Battle(player, enemy, actions, difficulty);

        Button shootButton = findViewById(R.id.shootButton);
        battle.startGame();

        shootButton.setOnClickListener(view -> {
            battle.loadProgress(0);
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void addResultToDb(int winner) {
        Results.ResultsReaderDbHelper dbHelper = new Results.ResultsReaderDbHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues result = new ContentValues();
        result.put(Results.ScoreEntry.COLUMN_NAME_WINNER, winner);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            result.put(Results.ScoreEntry.COLUMN_NAME_DATE, LocalDateTime.now().toString());
        }
        result.put(Results.ScoreEntry.COLUMN_NAME_LEVEL, difficulty.value);

        db.insert(Results.ScoreEntry.TABLE_NAME, null, result);
        db.close();
    }

    private void goToMainScreen() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

}