package com.umg.trains;

import static com.umg.trains.utils.Constants.DIFFICULTY.EASY;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.umg.trains.results.ResultActivity;
import com.umg.trains.utils.Constants;
import com.umg.trains.utils.Utils;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private boolean appExited = false;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private ImageView photo;
    private DrawerLayout mDrawer;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button startButton = findViewById(R.id.startButton);
        startButton.setOnClickListener(view -> startGame());

        photo = findViewById(R.id.takenPhoto);

        ActivityResultLauncher<Intent> someActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            if (data != null) {
                                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                                photo.setImageBitmap(bitmap);
                            }

                        }
                    }
                });

        Button photoButton = findViewById(R.id.takePhoto);
        photoButton.setOnClickListener(view -> someActivityResultLauncher.launch(new Intent(MediaStore.ACTION_IMAGE_CAPTURE)));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView nvDrawer = (NavigationView) findViewById(R.id.nvView);
        setupDrawerContent(nvDrawer);
        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(com.android.car.ui.R.drawable.car_ui_icon_overflow_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        selectDifficulty();
    }

    public void selectDrawerItem(MenuItem menuItem) {
        Intent intent = null;
        switch (menuItem.getItemId()) {
            case R.id.board:
                intent = new Intent(getApplicationContext(), ResultActivity.class);
                break;
            case R.id.home:
                intent = new Intent(getApplicationContext(), MainActivity.class);
                break;
        }

        if (intent != null)
            startActivity(intent);

        menuItem.setChecked(true);
        mDrawer.closeDrawers();
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("ZDJĘCIE ZORBIONE " + resultCode + " " + requestCode);

        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            photo.setImageBitmap(bitmap);
        }
    }

    public void startGame() {
        Intent intent = new Intent(getApplicationContext(), ChooseTrain.class);
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        this.appExited = true;
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (appExited)
            Toast.makeText(getApplicationContext(), getString(R.string.resume), Toast.LENGTH_LONG).show();

        this.appExited = false;
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        System.out.println("ON DESTROY");
        super.onDestroy();
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        Constants.DIFFICULTY level = null;

        switch (view.getId()) {
            case R.id.radioEasy:
                if (checked) {
                    level = EASY;
                }
                break;
            case R.id.radioNormal:
                if (checked)
                    level = Constants.DIFFICULTY.MEDIUM;
                break;
            case R.id.radioDifficult:
                if (checked)
                    level = Constants.DIFFICULTY.HARD;
                break;
            default:
                level = Constants.DIFFICULTY.MEDIUM;
        }

        Utils.saveDifficulty(getApplicationContext(), level);

    }

    private void selectDifficulty() {
        Constants.DIFFICULTY level = Utils.readDifficulty(getApplicationContext());
        RadioButton radioButton = null;
        switch (level) {
            case EASY:
                radioButton = (RadioButton) findViewById(R.id.radioEasy);
                break;
            case MEDIUM:
                radioButton = (RadioButton) findViewById(R.id.radioNormal);
                break;
            case HARD:
                radioButton = (RadioButton) findViewById(R.id.radioDifficult);
                break;
        }

        radioButton.setChecked(true);
    }

}