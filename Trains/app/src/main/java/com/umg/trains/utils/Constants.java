package com.umg.trains.utils;

public class Constants {

    public static enum DIFFICULTY {
        EASY(1),
        MEDIUM(2),
        HARD(3);

        public final int value;

        private DIFFICULTY(int value) {
            this.value = value;
        }

    }
}
