package com.umg.trains.battle;

public interface IBattle {

    void onBattleEnd(int winner);

}
