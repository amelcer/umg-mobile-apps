package com.umg.trains.battle;

import android.os.CountDownTimer;

import com.umg.trains.utils.Constants;

public class Battle {
    private final int playerId = 0;
    private final int enemyId = 1;
    private final Player[] players = new Player[2];
    private boolean gamOver;
    private int winner;
    private CountDownTimer timer;
    IBattle battleActions;
    Constants.DIFFICULTY difficulty;

    public Battle(Player player, Player enemy, IBattle actions, Constants.DIFFICULTY difficulty) {
        players[playerId] = player;
        players[enemyId] = enemy;
        gamOver = false;
        battleActions = actions;
        this.difficulty = difficulty;
    }

    public void startGame() {
        enemyAI();
    }

    public void stopGame() {
        timer.cancel();
        battleActions.onBattleEnd(winner);
    }

    private void enemyAI() {
        int attackSpeed = players[enemyId].getTrain().getAttackSpeed() * difficulty.value;
        timer = new CountDownTimer(10000, 700 / attackSpeed) {
            @Override
            public void onTick(long l) {
                loadProgress(enemyId);
            }

            @Override
            public void onFinish() {
                if (!gamOver)
                    enemyAI();
            }
        }.start();
    }

    public void loadProgress(int player) {
        players[player].updateProgress(players[player].getTrain().getDamage());

        if (players[player].isLoaded()) {
            shoot(player);
            players[player].resetProgress();
        }
    }

    public void shoot(int attacker) {
        int victim = attacker == playerId ? enemyId : playerId;
        boolean playerDead = players[attacker].attack(players[victim]);

        if (playerDead) {
            winner = attacker;
            gamOver = true;
            stopGame();
        }
    }

}
