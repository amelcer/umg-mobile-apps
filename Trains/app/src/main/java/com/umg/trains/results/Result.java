package com.umg.trains.results;

import android.os.Build;

import java.time.LocalDateTime;

public class Result {

    private LocalDateTime date;
    private int winner;
    private int level;

    public Result(int level, int winner, String dt) {
        this.level = level;
        this.winner = winner;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            this.date = LocalDateTime.parse(dt);
        }
    }

    public int getLevel() {
        return level;
    }

    public LocalDateTime getDate() {
        return date;
    }


    public boolean isPlayerWon() {
        return winner == 0;
    }

    public String getLevelName() {
        switch (level) {
            case 1:
                return "Łatwy";
            case 2:
                return "Normalny";
            case 3:
                return "Trudny";
            default:
                return "";
        }
    }

}
