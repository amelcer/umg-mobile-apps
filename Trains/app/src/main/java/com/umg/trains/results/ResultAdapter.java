package com.umg.trains.results;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.umg.trains.R;

import java.time.format.DateTimeFormatter;
import java.util.List;

public class ResultAdapter extends ArrayAdapter<Result> {

    DateTimeFormatter dateFormatter;

    public ResultAdapter(@NonNull Context context, @NonNull List<Result> objects) {
        super(context, 0, objects);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        }
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Result result = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.result_row, parent, false);
        }

        TextView rowNo = (TextView) convertView.findViewById(R.id.rowNo);
        int lp = position + 1;
        rowNo.setText(Integer.toString(lp));

        TextView winner = (TextView) convertView.findViewById(R.id.playerWon);
        String win = result.isPlayerWon() ? "Gracz" : "Komputer";
        winner.setText(win);

        TextView dt = (TextView) convertView.findViewById(R.id.date);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            dt.setText(dateFormatter.format(result.getDate()));
        }

        TextView lev = (TextView) convertView.findViewById(R.id.level);
        lev.setText(result.getLevelName());

        return convertView;
    }
}
