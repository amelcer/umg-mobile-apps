package com.umg.trains.results;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.umg.trains.R;
import com.umg.trains.db.Results;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {


    private ArrayList<Result> results;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        results = new ArrayList<Result>();
        getResults();
        printResults();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void getResults() {
        Results.ResultsReaderDbHelper dbHelper = new Results.ResultsReaderDbHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        @SuppressLint("Recycle") Cursor cursor = db.query(
                Results.ScoreEntry.TABLE_NAME,   // The table to query
                null,             // The array of columns to return (pass null to get all)
                null,              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                null               // The sort order
        );


        while (cursor.moveToNext()) {
            int level = cursor.getInt(cursor.getColumnIndexOrThrow(Results.ScoreEntry.COLUMN_NAME_LEVEL));
            int winner = cursor.getInt(cursor.getColumnIndexOrThrow(Results.ScoreEntry.COLUMN_NAME_WINNER));
            String date = cursor.getString(cursor.getColumnIndexOrThrow(Results.ScoreEntry.COLUMN_NAME_DATE));
            Result res = new Result(level, winner, date);

            results.add(res);
        }
        cursor.close();
    }

    private void printResults() {
        ResultAdapter adapter = new ResultAdapter(this, results);
        ListView resultsView = (ListView) findViewById(R.id.resultList);

        resultsView.setAdapter(adapter);
    }

}