package com.umg.trains.utils;

import android.content.Context;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Random;

public class Utils {

    private static final String difficultyLevelFileName = "difficulty";

    public static int getRandomNumber(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

    public static Constants.DIFFICULTY readDifficulty(Context context) {
        try {
            File f = new File(context.getFilesDir(), difficultyLevelFileName);
            RandomAccessFile raf = new RandomAccessFile(f, "rw");
            raf.seek(0);

            String level = raf.readUTF();

            return Constants.DIFFICULTY.valueOf(level);

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return Constants.DIFFICULTY.MEDIUM;
    }

    public static void saveDifficulty(Context context, Enum<Constants.DIFFICULTY> level) {
        try {
            File f = new File(context.getFilesDir(), difficultyLevelFileName);
            RandomAccessFile raf = new RandomAccessFile(f, "rw");
            raf.writeUTF(String.valueOf(level));
            raf.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
